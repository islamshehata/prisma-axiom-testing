import { setupOtel } from "../../lib/otel"
setupOtel()
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient()

export default async function handler(req, res) {
    await prisma.post.deleteMany()
    await prisma.user.deleteMany()
    const user = await prisma.user.create({
        data: {
            name: 'Alice',
            email: 'alice@prisma.io',
        },
    })
    console.log('created user', user)
    await prisma.user.findMany()

    await prisma.user.create({
        data: {
          name: 'Bob',
          email: 'bob@prisma.io',
          posts: {
            create: {
              title: 'Hello World',
            },
          },
        },
      })

    await prisma.user.findMany({
        include: {
          posts: true,
        },
      })

    await prisma.post.deleteMany()
    await prisma.user.deleteMany()

    await prisma.$disconnect()
    res.status(200).json({ msg: 'traces should be sent' })
}