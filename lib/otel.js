const { trace } = require('@opentelemetry/api');
import { SemanticResourceAttributes } from '@opentelemetry/semantic-conventions';
import { OTLPTraceExporter } from '@opentelemetry/exporter-trace-otlp-proto';
import { registerInstrumentations } from '@opentelemetry/instrumentation';
import { BatchSpanProcessor } from '@opentelemetry/sdk-trace-base';
import { NodeTracerProvider } from '@opentelemetry/sdk-trace-node';
import { PrismaInstrumentation } from '@prisma/instrumentation';
import { Resource } from '@opentelemetry/resources';

export function setupOtel() {
    const exporter = new OTLPTraceExporter({
        url: 'http://a7053d89739984969a18d2df3369c5c2-1663434110.us-east-2.elb.amazonaws.com:4318',
        concurrencyLimit: 10,
    });

  const provider = new NodeTracerProvider({
    resource: new Resource({
      [SemanticResourceAttributes.SERVICE_NAME]: 'prisma-axiom-testing',
    }),
  });

  provider.addSpanProcessor(new BatchSpanProcessor(exporter));

  registerInstrumentations({
    tracerProvider: provider,
    instrumentations: [new PrismaInstrumentation()],
  });

  trace.setGlobalTracerProvider(provider);
  provider.register();
  return provider;
}
